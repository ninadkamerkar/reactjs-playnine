import React, { Component } from "react";

let _ = require('underscore');

const Stars = (props) => {
	return (
		<div>{
	            _.range(props.randomStars).map( (number) => 
	            	<span key={number} className="stars">*</span>)
            }
            <br/>
    		<br/>
            <button onClick={props.refreshStars} disabled={props.redraws === 0}># {props.redraws}</button>			
		</div>
	);
}

const Button = (props) => {

    const changeClassName = () => {
        if(props.answerIsCorrect === true)  {
            return "btn-success";
        } else if(props.answerIsCorrect === false)  {
            return "btn-danger";
        } else {
            return "btn-default";
        }
    }

    return (
    	<div>
            <button 
                onClick={props.checkAnswer} 
                className={changeClassName()}
                disabled={props.selectedNumbers.length === 0}>=</button>
    	</div>
	);
}

const Answer = (props) => {
    return (
        <div>{
        	props.selectedNumbers.map( (number) => 
        	<button 
        		key={number} 
        		className="btn-default"
        		onClick={() => props.unselectNumber(number)}>{number}</button>)
        }
        </div>
    );
}

const Numbers = (props) => {

	const changeClassName = (number) => {
		if(props.usedNumbers.includes(number)) {
			return "btn-danger";
		} else if(props.selectedNumbers.includes(number)) { 
			return "btn-success";
		} else {
			return "btn-default";
		}
	}

	const disableNumber = (number) => {
		if(props.usedNumbers.includes(number)) {
			return true;
		} else if(props.selectedNumbers.includes(number)) { 
			return true;
		} else {
			return false;
		}
	}

    return (
        <div>{
        Numbers.list.map( (number) => 
            	<button key={number} className={changeClassName(number)} 
            	onClick={() => props.selectNumbers(number)}
            	disabled={disableNumber(number)}>
            	{number}</button>
        	)
        }	
        </div>
    );

}

Numbers.list = _.range(1,10);

const Result = (props) => {
    return (
        <div>
        	<h4 className="result">{props.output}</h4>
        </div>
    );
}

export class PlayNine extends Component {

	static initial_state = () => ({
		stars: PlayNine.randomNumber(),
		redraws: 5,
		selectedNumbers: [],
		usedNumbers: [],
		answerIsCorrect: null,
		result: 'You can do it !!!'		
	})

	state = PlayNine.initial_state();

	resetState = () => {
		this.setState(
			PlayNine.initial_state()
		);
	}	

	static randomNumber = () => Math.floor(Math.random()*9) + 1;

	refreshStars = () => {
		if(this.state.redraws > 0) {
			const attemptsLeft = this.state.redraws - 1;
			console.log("attemptsLeft = "+attemptsLeft)
			this.setState((prevState) => 
					{ 
						return {
							stars: PlayNine.randomNumber(),
							redraws: prevState.redraws -1
						}
					},
					() => this.checkFinalResult()
			);

		} else {
			console.log("Redraws Over");
			this.checkFinalResult();
		}
	}

	selectNumbers = (number) => {
		this.setState({selectedNumbers: [...this.state.selectedNumbers,number]});
	}

	unselectNumber = (number) => {
		console.log("unselectNumber "+number);
	  	this.setState({selectedNumbers: [...this.state.selectedNumbers,number].filter(num => num !== number),answerIsCorrect:null});
	}

	checkAnswer = () => {
		if(this.state.answerIsCorrect === null || this.state.answerIsCorrect === false)   {
			const ans = this.state.selectedNumbers.reduce((acc, n) => acc + n, 0);
			const stars = this.state.stars;
			//console.log(ans+"="+stars);
			if(ans === stars) {
				this.setState({answerIsCorrect:true});
			} else {
				this.setState({answerIsCorrect:false});
			}
		} else {
			this.setState({answerIsCorrect:null,
				usedNumbers: this.state.usedNumbers.concat(this.state.selectedNumbers),
				selectedNumbers: [],stars: PlayNine.randomNumber()}
				,() => this.checkFinalResult());
		}
	}

	checkFinalResult = () => {
		console.log("checkFinalResult Redraws = "+this.state.redraws);
		const pendingArr = _.range(1,10).filter( item => !this.state.usedNumbers.includes(item));
		let hasChance = null;

		//All numbers finished
		if(pendingArr.length === 0) {
			this.setState({result:'WON'});
			hasChance = false;
		} else {
			//Has redraws
			if(this.state.redraws > 0 && pendingArr.length !== 0) {
				hasChance = true;
			} else {
				if(pendingArr.length === 1 && !pendingArr.includes(this.state.stars)) {
					hasChance = false;
				} else if(this.state.stars === 1 && !pendingArr.includes(1)) {
					hasChance = false;
				} else if(pendingArr.includes(this.state.stars)) {
					hasChance = true;
				} else {
					//Only taking pending numbers which are less than stars
					//as numbers greater than stars is not a solution
					const newArr = pendingArr.filter(item => item < this.state.stars);
					if(newArr.length === 0)  {
						hasChance = false;
					} else if(newArr.length === 1 && newArr[0] !== this.state.stars )  {
						hasChance = false;
					} else if(pendingArr.reduce((acc, n) => acc + n, 0) === this.state.stars) {
						hasChance = true;
					} else {
						for (var i = 0; i < newArr.length; i++) {
							let I_NUM = newArr[i];
							let subsetArr = newArr.filter(item => item !== I_NUM);
							for (var j = 0; j <= subsetArr.length; j++) {
								let spliceArr = [...subsetArr].splice(j,subsetArr.length);
								
								let sum = spliceArr.reduce((acc, n) => acc + n, 0) + I_NUM;

								console.log(I_NUM+","+spliceArr+" = Sum("+sum+") = Stars("+this.state.stars+") ?");
								if(sum === this.state.stars) {
									hasChance = true;
									break;
								}
								console.log("--------------------");
							}
							if(hasChance)
								break;
						}
					}
				}
			}

			if(!hasChance) {
				this.setState({result:'LOST'});
			}
		}
		this.setState({won:hasChance});	
	}


	componentDidMount = () => {
		//this.checkFinalResult();
	}

    render() {
        return (
	        <div className="container playnine">
	        	<div className="row title">
	        		<div className="col-12">
	        			Play Nine
	        		</div>
	        	</div>
	        	<hr/>
	        	<div className="row">
	        		<div className="col-4">
						<Stars 
	                		randomStars={this.state.stars} 
	                		redraws={this.state.redraws} 
	                		refreshStars={this.refreshStars}/>
	        		</div>
	        		<div className="col-4">
		                <Button 
		                	checkAnswer={this.checkAnswer} 
		                	answerIsCorrect={this.state.answerIsCorrect}
		                	selectedNumbers={this.state.selectedNumbers}/>
	        		</div>
	        		<div className="col-4">
		                <Answer 
		                	selectedNumbers={this.state.selectedNumbers}
		                	unselectNumber={this.unselectNumber}/>
	        		</div>
	        	</div>
	        	<hr/>
		       	<div className="row">
	        		<div className="col-12">
						<Numbers 
							selectNumbers={this.selectNumbers}
							selectedNumbers={this.state.selectedNumbers}
		                	usedNumbers={this.state.usedNumbers}/>
	        		</div>
	        	</div>  
		       	<div className="row">
	        		<div className="col-12">
	        			<br/>
	        			<button className="btn-success" onClick={this.resetState}>Reset</button>
	        			<br/>
			        	<Result output={this.state.result}/>
	        		</div>
	        	</div>  
	        </div>
        );
    }
}