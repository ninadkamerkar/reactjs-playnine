import React, { Component } from 'react';
import './App.css';

import { PlayNine } from './PlayNine.js'

class App extends Component {
  render() {
    return (
      <div className="App">
        <PlayNine/>
      </div>
    );
  }
}

export default App;
